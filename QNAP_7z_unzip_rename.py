import os
import time
import py7zr
import PySimpleGUI as sg

sg.theme('Dark Blue 3')

file_list_column = [
    [
        sg.Text("解壓縮根目錄：", size=(12, 1)),
        sg.In(size=(50, 1), key="-FOLDER-", enable_events=True),
        sg.FolderBrowse(button_text='選擇根目錄資料夾')
    ],
    [
        sg.Text("解壓縮密碼：", size=(12, 1)),
        sg.Input(size=(50, 1), key="-PASSWORD-"),
        
    ],
    [
        sg.Checkbox(
        '我需要指定解壓後的檔案根目錄到其它位置',
        default=False, key="-OTHER_EXPORT-",
        enable_events=True),
    ],
    [
        sg.Text("解壓縮輸出根目錄：", size=(15, 1), text_color ='gray', key="-TEXT_EXPORT-"),
        sg.In(size=(50, 1), key="-FOLDER_OUTPUT-", disabled=True),
        sg.FolderBrowse(button_text='選擇輸出根目錄資料夾', disabled=True, key="-BROWSE_EXPORT-")
    ],
    [sg.Button('開始解壓縮', size=(12, 1))],
    [
        sg.Output(
            size=(100, 20)
        )
    ],
]

layout = [
    [
        sg.Column(file_list_column),
    ]
]

window = sg.Window("QNAP批次修改被壓縮的亂碼檔名視窗化程式 ver.20210510 by scku208", layout)

# Run the Event Loop
while True:
    event, values = window.read()
    if event == "Exit" or event == sg.WIN_CLOSED:
        break
    if event == "-OTHER_EXPORT-":
        if values["-OTHER_EXPORT-"] == True:
            window['-TEXT_EXPORT-'].update(text_color='white')
            window['-FOLDER_OUTPUT-'].update(text_color='black')
            window['-FOLDER_OUTPUT-'].update(disabled=False)
            window['-BROWSE_EXPORT-'].update(disabled=False)
        else:
            window['-TEXT_EXPORT-'].update(text_color='gray')
            window['-FOLDER_OUTPUT-'].update(text_color='gray')
            window['-FOLDER_OUTPUT-'].update(disabled=True)
            window['-BROWSE_EXPORT-'].update(disabled=True)
    if event == "-FOLDER-":
        if values["-OTHER_EXPORT-"] == True:
            pass
        else:
            window['-FOLDER_OUTPUT-'].update(value=values['-FOLDER-'], text_color='gray')
    if event == "開始解壓縮":
        STOP = False
        if values["-OTHER_EXPORT-"] == True:
            dest_r = values['-FOLDER_OUTPUT-']
        else:
            dest_r = values['-FOLDER-']
        for r_, d_, files in os.walk(values['-FOLDER-']):
            for f_ in files:
                if f_.endswith('.7z'):
                    try:
                        print(f'處理壓縮檔… {os.path.join(r_, f_)}...')
                        # correct_name = f_.rsplit('.', 1)[0]
                        archive = py7zr.SevenZipFile(
                            os.path.join(r_, f_), mode='r',
                            password=values['-PASSWORD-'])
                        # correct name can be accqured from wrong name now
                        wrong_name = archive.getnames()[0]
                        correct_name = wrong_name.encode('raw_unicode_escape').decode('utf8')

                        try:
                            mod_t = time.mktime(archive.list()[0].creationtime.astimezone().timetuple())
                            bio = archive.readall()[wrong_name]
                            archive.close()
                        except (py7zr.exceptions.CrcError, py7zr.exceptions.Bad7zFile) as e:
                            print('\t解壓失敗?!，是不是打錯密碼?!')
                            print('\t程式已停止')
                            STOP = True
                            break

                        if os.path.exists(os.path.join(r_.replace(values['-FOLDER-'], dest_r), correct_name)):
                            print('\t解壓後檔案已存在，怕覆蓋重要檔案，程式將略過')
                        else:
                            os.makedirs(r_.replace(values['-FOLDER-'], dest_r), exist_ok=True)
                            correct_file_path = os.path.join(r_.replace(values['-FOLDER-'], dest_r), correct_name)
                            with open(correct_file_path, 'wb') as correct_file:
                                correct_file.write(bio.read())
                            os.utime(correct_file_path, (mod_t, mod_t))
                            print('\t解壓成功!!')

                    except Exception as e:
                        print('\t!!不好意思，程式因未考慮到的錯誤而停止，有需要請與管理員連絡並提供相關的錯誤訊息 scku208@gmail.com，謝謝。')
                        print(f'\t相關錯誤訊息：{e}')
                        STOP = True
                if STOP:
                    break
            if STOP:
                break
        print('==程式執行結束==')
window.close()